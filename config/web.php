<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
	'homeUrl' => ['lead/index'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'fcQqVMS8zR7nZuD5pXbqPegf2oxZNhRZ',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
		'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],		

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,	
			'rules' => [
				'users' => 'user/index',
				'user/<id:\d+>' => 'user/view',
				'user/update/<id:\d+>' => 'user/update',
				'user/delete/<id:\d+>' => 'user/delete',				
				'leads' => 'lead/index',
				'deals' => 'deal/index',
				'deal/update/<id:\d+>' => 'deal/update',
				'lead/<id:\d+>' => 'lead/view',
				'lead/update/<id:\d+>' => 'lead/update',
				'lead/delete/<id:\d+>' => 'lead/delete',				
				'bonus' => 'bonus/index',
				'bonus/<id:\d+>' => 'bonus/view',
				'bonus/update/<id:\d+>' => 'bonus/update',
				'bonus/delete/<id:\d+>' => 'bonus/delete',
			],
		],
    ],
    'params' => $params,
];


if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
         'allowedIPs'=>['*.*.*.*']
    ];
}


return $config;
