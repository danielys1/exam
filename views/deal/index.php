<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Lead;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            //'leadId',
           
             [
				'attribute' => 'leadId',
				'label' => 'LeadId',
				'format' => 'raw',
				'value' => function($model){
					return $model->leadn->name;
				},
				//'filter'=>Html::dropDownList('DealSearch[leadId]', $leadId, $leads, ['class'=>'form-control']),
			],			
       
            'name',
            'amount',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>