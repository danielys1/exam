<?php

namespace app\models;

use Yii;
use yii\validators\NumberValidator;
/**
 * This is the model class for table "bonus".
 *
 * @property integer $Id
 * @property integer $userId
 * @property integer $reasonId
 * @property integer $amount
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'userId', 'reasonId', 'amount'], 'required'],
            [['Id', 'userId', 'reasonId', 'amount'], 'integer'],
            [['amount'] , 'integer' ,'min'=>100 ,'max'=>1000],



   ];

      
        
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'userId' => 'User ID',
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
        ];
    }
}
